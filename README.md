# uuidUrl
uuidUrl browser app that can shorten links. It uses NodeJS, uuid and other nodejs libraries. <br>
This is not meant to revolutionize the world. This is done with exercise in mind. <br>

uuidUrl is licensed under MIT License.

## Installation
### Arch Linux:
If you have enabled my [op-arch-repo](https://gitlab.com/orasponka/op-arch-repo) you can install uuidUrl via pacman.
```
sudo pacman -Sy uuidurl
```
If not, do things below. You must have installed base-devel on your system.
```
git clone https://gitlab.com/orasponka/uuidurl.git
cd uuidurl
makepkg
sudo pacman -U *.zst
```
## Usage
Open uuidurl to local network on port 80.
```
uuidurl
```
Open uuidurl to localhost on port 8080
```
uuidurl --local 
or
uuidurl -l
```
Change port via --port flag
```
uuidurl --port 5555
or
uuidurl -p 5555
```
Do not save any data or use any data in /data
```
uuidurl --ignore-saved-data
or
uuidurl -i
```
