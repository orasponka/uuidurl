document.addEventListener('click', e => {
  var classList = e.target.classList
  if(classList != null && classList != undefined){
    for(var i = 0;i < classList.length;i++){
      var elemClass = classList[i]
      if(elemClass == "copy"){
        copyAlert(e.target.id)
      }
    }
  }
})

function copyAlert(id){
  var copyTextElems = document.getElementsByClassName("copyText");

  for (var i = 0;i < copyTextElems.length;i++){
    var elem = copyTextElems[i]
    if(elem.id == id){
      elem.innerText = "Kopioitu!"
      elem.style.display = "inline-block"
    } else {
      elem.style.display = "none"
      elem.innerText = ""
    }
  }
}
