var local = false
var customPortBoolean = false
var customPort;
var ignore = false;
process.argv.forEach((val, index, array) => {
  if (val == "--local" || val == "-l"){
    local = true
  }

  if (val == "--port" || val == "-p"){
    customPort = process.argv[index + 1]
    customPortBoolean = true
  }

  if (val == "--ignore-saved-data" || val == "-i"){
    ignore = true
  }
})

const express = require("express")
const app = express()
const server = require('http').Server(app)
var host;
var port;
if (local) {
  host = 'localhost'
  port = 8080
} else {
  host = '0.0.0.0'
  port = 80
}

if (customPortBoolean) {
  port = customPort
}

server.listen(port, host)
const io = require('socket.io')(server)
const cors = require('cors')
const fs = require('fs')
const path = require('path')

app.set('views', './views')
app.set('view engine', 'ejs')
app.use(express.static('public'))
app.use(express.urlencoded({ extended: true }))
app.use(
    cors({
        origin: "*",
    })
)

var dataJSON = fs.readFileSync(path.resolve(__dirname, 'data/data.json'));

var urlList = JSON.parse(dataJSON).urlList;
var uuidList = JSON.parse(dataJSON).uuidList;

if (ignore){
  urlList = []
  uuidList = []
}

var hostIp;
if (host == 'localhost'){
  hostIp = 'localhost'
} else {
  var hostIp = require('ip').address().toString()
}

console.log(hostIp + ':' + port)

app.post('/shorturl', (req, res) => {
  const url = req.body.url
  const uncutUuid = require('uuid').v4()
  const uuid = uncutUuid.slice(0,8)

  urlList.push(url)
  uuidList.push(uuid)

  writeData()

  return res.redirect('/home')
})

app.post('/delete', (req, res) => {
  const uuid = req.body.uuid
  if (existInList(uuid, uuidList)){
    const index = uuidList.indexOf(uuid)
    urlList.splice(index, 1)
    uuidList.splice(index, 1)
    writeData()
  }
  return res.redirect('/home')
})

app.get('/home', (req, res) => {
  res.render('index', { data : { urlList : urlList, uuidList : uuidList, ip : hostIp, port : port } })
})

app.get('*', (req, res) => {
  res.status(301).redirect(getURL(req.url));
})

function getURL(uuid) {
  var urlToReturn = '/home'

  uuidList.forEach(item => {
    if ("/" + item == uuid) {
      urlToReturn = urlList[uuidList.indexOf(item)];
    }
  })

  return urlToReturn;
}

function writeData(){
  if(!ignore){
    var obj = { urlList : urlList, uuidList : uuidList}
    fs.writeFileSync('./data/data.json', JSON.stringify(obj, null, 2) , 'utf-8');
  }
}

function existInList(query, list){
  var returnValue = false;
  list.forEach(item => {
    if (item == query){
      returnValue = true
    }
  })
  return returnValue;
}
